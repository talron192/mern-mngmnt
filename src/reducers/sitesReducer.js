const initState = {
    customerData: {},
    bankList: [],
    msg: 'hello tal',
    fileIsExist:false,
    eventIsDone:false,
    closeModal:false
}

const sitesReducer = (state = initState, action) => {

    switch (action.type) {
        case "GET_CUSTOMER_DATA":
            state = { ...state, customerData: action.payload }
            break;
        case "GET_BANK_LIST":
            console.log('GET_BANK_LIST',action.payload);

            state = { ...state, bankList: action.payload }
            break;

        case "GET_MSG":
            state = { ...state,msg: action.payload }

        case "FILE_IS_EXIST":
            state = {...state,fileIsExist: action.payload}
            console.log('siteReducer-FILE_IS_EXIST',state);
            break;
        
        case "EVENT_IS_DONE":
            state = {...state,eventIsDone: action.payload}
            console.log('siteReducer-EVENT_IS_DONE',state);
            break;

            case "CLOSE_MODAL":
            state = {...state,closeModal: action.payload}
            console.log('siteReducer-CLOSE_MODAL',state);
            break;

        default:
            break;
    }
    console.log('sitesReducer', state);
    return state;
}

export default sitesReducer;