import React from '../../node_modules/react';
import SignaturePad from '../../node_modules/react-signature-canvas'
import '../components/style.css';
import axios from "axios";
import {connect} from "react-redux";
import {closeModal, fileIsExist, getMsg} from "../actions/customerActions";
import Alert from '../../node_modules/react-bootstrap/Alert';
import "../images/mizrahi_logo.png";


class mizrahi_template extends React.Component {
    sigPad = {};
    state = {trimmedDataURL: null, resMsg: ''}
    clear = () => {
        this.sigPad.clear()
    }
    trim = () => {
        this.setState({
            trimmedDataURL: this.sigPad.getTrimmedCanvas()
                .toDataURL('image/png')
        })
    }

    handleSubmit = () => {
        const newReceipt = {
            eventID: Math.random(),
            fullName: this.props.state.obj.fullName,
            _id: this.props.state.obj._id,
            customerSig: this.state.trimmedDataURL,
            templateName: 'PowerAttorney',
            bankName: 'Mizrahi'

        };
        console.log('newReceipt', newReceipt);

        axios.post('http://localhost:4000/customers/addTemplateFile/' + newReceipt._id, newReceipt)
            .then(res => {
                this.setState({
                    msgEvent: res.data,
                    showReciept: true,
                    showPopUp: true,
                    resMsg: 'נשמר בהצלחה'
                })
                this.props.close_modal(true);

            })
            .catch(err => {
                console.log(err);
            });
    }


    render() {
        let {trimmedDataURL} = this.state
        let mizrahiLogo = require('../images/mizrahi_logo.png');

        return (
            <div id="ProdReceipt" className="container" style={{position: "absolute", textAlign: "right"}}>
                {/*<div className="row">*/}
                {/*    <div className="col-md-12">*/}
                {/*        <img src={mizrahiLogo}></img>*/}

                {/*    </div>*/}
                {/*</div>*/}
                <h3 style={{textAlign: 'center'}}>
                    <u>
                        כתב הסכמה ליועץ משכנתאות
                    </u>
                </h3>

                <div className="row" style={{paddingBottom: " 1em"}}>
                    <div className="col-md-12">

                        <section>1. אנו הח"מ</section>
                        <table style={{border: '1px solid black', width: '50%'}}>
                            <thead>
                            <tr>
                                <th style={{border: '1px solid black'}}>שם</th>
                                <th style={{border: '1px solid black'}}>מספר מזהה (ת.ז., דרכון)</th>
                                <th style={{border: '1px solid black'}}>פרטי התקשרות</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id="fullName"
                                    style={{border: '1px solid black'}}>{this.props.state.obj.fullName}</td>
                                <td id="id" style={{border: '1px solid black'}}>{this.props.state.obj._id}</td>
                                <td id="id" style={{border: '1px solid black'}}>{this.props.state.obj._id}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="row" style={{paddingBottom: " 1em"}}>
                    <div className="col-md-12">
                        <h5>מסמיכים את</h5>
                        <table style={{border: '1px solid black', width: '50%'}}>
                            <thead>
                            <tr>
                                <th style={{border: '1px solid black'}}>שם</th>
                                <th style={{border: '1px solid black'}}>מספר מזהה (ת.ז., דרכון)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id="id" style={{border: '1px solid black'}}>צחי כהן</td>
                                <td id="id" style={{border: '1px solid black'}}>123456789</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div>
                    שיקראו להלן: "המורשים",להוציא לפועל ו/או לחתום על כל או איזה מן הפעולות, הדברים ,החוזים ו/או
                    המסמכים כדלקמן :
                </div>
                <section id="textarea">
                    <ol>
                        <section>1.1 לנהל בשמנו ובמקומנו משא ומתן לצורך קבלת ההלוואה(להלן - <b>"בקשת הלוואה"</b>),בין
                            הלוואה חדשה ובין הלוואה המיועדת לסילוק הלוואת משכנתא קיימת.
                        </section>
                        <section> 1.2 לפנות אל נציג הבנק, להופיע בפניו, להיפגש עימו ולדון בתנאי ההלוואה השונים,
                            לרבות, בין
                            היתר, גובה הריבית, סוגי ההלוואה, תקופתה וכו'.
                        </section>
                        <section>1.3 לקבל מכל מקור שהוא, כולל מהבנק בו מוגשת בקשת ההלוואה, וכן מכל חברה או מעביד, כל
                            מידע שיש
                            בידיהם מכל מקור שהוא, ואשר ייראה למוסמכים על ידינו כדרוש לדיון בבקשת ההלוואה ו/או דרוש
                            כדי לאמת את הפרטים בבקשת ההלוואה, וכן להסמיך את הבנק לקבל מידע זה, ולוותר לצורך כך, על
                            סודיות המידע.
                            הננו מוותרים על זכויותינו לסודיות או לאחריות הבנק ביחס למידע המפורט לעיל, על-פי כל הסכם,
                            נוהג בנקאי ו/או דין, לרבות חוק הגנת הפרטיות התשמ"א-1981 ו/או כל חוק אחר.
                        </section>
                        <section>1.4 לקבל, לעיין ולצלם את כל המסמכים הדרושים מגופים שונים, לרבות, בין היתר, אך לא
                            רק, לשכת
                            רישום המקרקעין, רשות מקרקעי ישראל וחברות משכנות, לשם אישור בקשת ההלוואה ולשם ביצועה,
                            לרבות מסמכים שקבלתם מחויבת באגרה/עמלה/תשלום.
                        </section>
                        <section>1.5 להגיש בשמנו את כל המסמכים הנדרשים לצורך קבלת אישור עקרוני או הדחייה מהבנק
                            בעניין בקשת הלוואה.
                        </section>
                        <section>1.6 לחתום בשמנו ולקבל במקומנו את ההודעה על החלטת האשראי: האישור העקרוני, אם ניתן
                            לבקשת ההלוואה, המפרט את כל
                            תנאי ההלוואה, ובין היתר את שיעורי הריבית ומועד שמירתה או דחיית הבקשה.
                        </section>
                    </ol>
                    <section>2. אנו מצהירים כי איננו בגדר "לקוחות מוגבלים" ואין לנו "חשבון מוגבל" כמשמעותם בחוק
                        שיקים
                        ללא כיסוי התשמ"א-1981 ומסמיכים את המוסמכים להצהיר זאת בשמנו כלפי הבנק.
                    </section>
                    <section>3. אנו מצהירים כי לא הוטלו עיקולים כלשהם על הנכס המוצע כשעבוד להבטחת הלוואת המשכנתא
                        ו/או על
                        כספים ו/או על נכסים אחרים שלנו ומסמיכים את מורשינו להצהיר זאת בשמנו כלפי הבנק.
                    </section>
                    <section>4. המורשים יהיו רשאים, להצהיר בשמנו כלפי הבנק, כי ידוע לנו ואנו מסכימים שכל התקשרות בפועל
                        בינינו לבין הבנק שייתן לנו הלוואת משכנתא תהיה כפופה לתנאים הבאים :
                        <br></br>
                        <section>
                            <ul>
                                <li>אישור בקשת ההלוואה ע"י המורשים לכך בבנק.
                                </li>
                                <li>כל הפרטים שיימסרו לבנק על ידינו, בין ישירות על ידינו ובין באמצעות המורשים הנ"ל, הם
                                    נכונים וכל מסמך שהוגש לבנק כאמור הינו אמיתי, שלם ובר-תוקף
                                </li>
                                <li>מילוי כל התנאים והדרישות המצויינים ב"מדריך לקבלת הלוואה", שנמסר/יימסר לידינו ו/או
                                    לידי המורשים, וכן אלה שיפורטו באישור בקשת ההלואה שנקבל או יקבלו המורשים הנ"ל, וכן
                                    בהנחיות פקיד הבנק שיאשר את בקשת ההלוואה.
                                </li>
                                <li>תשלום כל ההוצאות הנדרשות לשם ביצוע ההלוואה.</li>
                            </ul>
                        </section>
                    </section>
                    <section>5. המורשים יהיו רשאים, להצהיר בשמנו כלפי הבנק, כי ידוע לנו ואנו מסכימים כי אם תאושר בקשת
                        ההלוואה, במלואה או בחלקה:
                        <br></br>
                        <section>
                            <ul>
                                <li>כל אישור הלוואה כאמור לא ישמש אלא הבעת נכונות עקרונית, והוא לא יטיל על הבנק שאישר את
                                    בקשת ההלוואה כל התחייבות ו/או אחריות כלשהן למתן ההלוואה, ורק האמור בהסכם הלוואה חתום
                                    ביננו לבין הבנק יחייב את הבנק.
                                </li>
                                <li>הבנק יהיה רשאי, בין השאר, לשנות את שיעור הריבית ו/או שיעור התוספת/הפחתה לבסיס
                                    הריבית, בכפוף לכללי תוקף שמירת הריבית, ו/או לשנות תנאים אחרים שיאושרו, וזאת עד למתן
                                    ההלוואה בפועל.
                                </li>
                            </ul>
                        </section>
                    </section>
                    <section>
                        6. יובהר כי אנו מודעים לכך כי המורשים אינם פועלים בשם הבנק והבנק אינו אחראי לפעולותם.
                    </section>


                </section>
                <div>
                    <div>
                        ולראיה באנו על החתום,
                        <br></br>
                        חתימת המבקש/ים:
                        <br></br>
                        <table style={{border: '1px solid black', width: '50%'}}>
                            <thead>
                            <tr>
                                <th style={{border: '1px solid black'}}>מס'</th>
                                <th style={{border: '1px solid black'}}>שם משפחה ופרטי/שם תאגיד.</th>
                                <th style={{border: '1px solid black'}}>חתימת המבקש/ים</th>
                                <th style={{border: '1px solid black'}}>תאריך</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id="fullName"
                                    style={{border: '1px solid black'}}>1</td>
                                <td id="id" style={{border: '1px solid black'}}>{this.props.state.obj.fullName}</td>
                                <td id="id" style={{border: '1px solid black'}}></td>
                                <td id="id" style={{border: '1px solid black'}}>date</td>
                            </tr>
                            </tbody>
                        </table>
                        <br></br>
                        חתימת היועץ.חברת יעוץ:
                        <br></br>
                        <table style={{border: '1px solid black', width: '50%'}}>
                            <thead>
                            <tr>
                                <th style={{border: '1px solid black'}}>מס'</th>
                                <th style={{border: '1px solid black'}}>שם משפחה ופרטי/שם תאגיד.</th>
                                <th style={{border: '1px solid black'}}>מספר מזהה וקוד מזהה</th>
                                <th style={{border: '1px solid black'}}>חתימת היועץ/חברת הייעוץ</th>
                                <th style={{border: '1px solid black'}}>תאריך</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id="fullName"
                                    style={{border: '1px solid black'}}>{this.props.state.obj.fullName}</td>
                                <td id="id" style={{border: '1px solid black'}}>צחי כהן</td>
                                <td id="id" style={{border: '1px solid black'}}>123456789</td>
                                <td id="id" style={{border: '1px solid black'}}>חתימה צחי</td>
                                <td id="id" style={{border: '1px solid black'}}>DATE</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <br></br>
                    <div>
                        <u><u>אימות חתימה ע"י עורך דין:</u></u>
                        <br></br>
                        אני מאשר בזאת את חתימת
                        <br></br>
                        <div className="row">
                            <div className="col-md-4">
                                שם: צחי כהן
                            </div>
                            <div className="col-md-4">
                                ת.ז: 123456789
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div className="row">
                            <div className="col-md-4">
                                תאריך:_________
                            </div>
                            <div className="col-md-4">
                                שם:_________
                            </div>
                            <div className="col-md-4">
                                חותמת/חתימה:______________
                            </div>
                        </div>

                    </div>
                    <br/>
                    <br/>
                    <div>
                        <u>אימות חתימה ע"י יועץ המשכנתאות החיצוני:</u>
                        <br></br>

                        אני מאשר בזאת את חתימת:
                        <br></br>
                        <div className="row">
                            <div className="col-md-4">
                                שם: צחי כהן
                            </div>
                            <div className="col-md-4">
                                ת.ז: 123456789
                            </div>

                        </div>
                        <br/>
                        <br/>
                        <div className="row">
                            <div className="col-md-4">
                                תאריך:___
                            </div>
                            <div className="col-md-4">
                                {/*שם:<input onChange={this.handleChange.bind(this)} placeholder="הקלד..."></input>*/}
                                שם :{this.props.state.obj.fullName}
                            </div>
                            <div className="col-md-4">
                                <div>
                                    <label style={{marginRight: '-5em'}}>
                                        חותמת\חתימה:
                                    </label>
                                    <div className='container_' style={{display: trimmedDataURL ? 'none' : ''}}>
                                        <div className='sigContainer'>
                                            <SignaturePad
                                                canvasProps={{width: 330, height: 175, className: 'sigCanvas'}}
                                                ref={(ref) => {
                                                    this.sigPad = ref
                                                }}/>
                                            <div style={{width: '20.7em'}}>
                                                <i onClick={this.clear} style={{cursor: 'pointer'}}
                                                   className="fa fa-times" aria-hidden="true"></i>
                                                <i onClick={this.trim}
                                                   style={{float: 'left', cursor: 'pointer'}}
                                                   className="fa fa-check" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    {trimmedDataURL
                                        ?
                                        <div className='container_image'>
                                            <img className="sigImage"
                                                 src={trimmedDataURL}/>
                                        </div>

                                        : null}
                                </div>

                            </div>
                        </div>
                        <br/>
                        <div className="row" style={{marginLeft: '28em'}}>
                            <div className="col-md-12">
                                <button onClick={this.handleSubmit.bind(this)} className="btn btn-secondary"
                                        style={{
                                            borderRadius: '1em',
                                            borderColor: '#f7b742',
                                            backgroundColor: '#f7b742',
                                            float: 'left',
                                            'width': '10em',
                                            'fontWeight': 'bold'
                                        }}>שמור
                                </button>
                            </div>
                        </div>
                        {
                            this.state.resMsg != '' ?
                                <div className="row">
                                    <div className="col-md-12">
                                        <Alert style={{textAlign: 'center'}} variant="success">
                                            <Alert.Heading>{this.state.resMsg}</Alert.Heading>
                                        </Alert>
                                    </div>
                                </div> : ''
                        }

                    </div>
                </div>

            </div>
        )
    }


}

const mapStateToProps = state => {
    console.log('mapStateToProps', state);
    return {
        closeModal: state.sitesReducer.closeModal

    }
}

const mapDispatchToProps = disaptch => {
    return {
        close_modal: (lanuchStatus) => {
            disaptch(closeModal(lanuchStatus));
        }

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(mizrahi_template);












