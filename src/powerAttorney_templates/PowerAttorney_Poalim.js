import React from 'react';
import SignaturePad from 'react-signature-canvas'
import '../components/style.css';
import axios from "axios";
import {connect} from "react-redux";
import {closeModal, fileIsExist, getMsg} from "../actions/customerActions";
import Alert from '../../node_modules/react-bootstrap/Alert';


class PowerAttorney_Poalim extends React.Component {
    sigPad = {};
    state = {trimmedDataURL: null, resMsg: ''}
    clear = () => {
        this.sigPad.clear()
    }
    trim = () => {
        this.setState({
            trimmedDataURL: this.sigPad.getTrimmedCanvas()
                .toDataURL('image/png')
        })
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    componentDidMount() {
        console.log(this.props.state);
    }

    handleSubmit = () => {
        const newReceipt = {
            eventID: Math.random(),
            fullName: this.props.state.obj.fullName,
            _id: this.props.state.obj._id,
            customerSig: this.state.trimmedDataURL,
            templateName: 'PowerAttorney',
            bankName: 'Poalim'

        };
        console.log('newReceipt', newReceipt);

        axios.post('http://localhost:4000/customers/addTemplateFile/' + newReceipt._id, newReceipt)
            .then(res => {
                this.setState({
                    msgEvent: res.data,
                    showReciept: true,
                    showPopUp: true,
                    resMsg: 'נשמר בהצלחה'
                })
                this.props.close_modal(true);

            })
            .catch(err => {
                console.log(err);
            });
    }


    render() {
        let {trimmedDataURL} = this.state

        return (
            <div id="ProdReceipt" className="container" style={{position: "absolute", textAlign: "right"}}>
                <div>
                    <h3 style={{textAlign: 'center'}}>
                        <u>
                            כתב הרשאה ליועץ משכנתאות חיצוני
                        </u>
                    </h3>
                    <div className="row" style={{paddingBottom: " 1em"}}>
                        <div className="col-md-12">
                            <h5>אנו הח"מ</h5>
                            <table style={{border: '1px solid black', width: '50%'}}>
                                <thead>
                                <tr>
                                    <th style={{border: '1px solid black'}}>שם</th>
                                    <th style={{border: '1px solid black'}}>מספר ת.ז</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    {/*<td id="fullName" style={{border: '1px solid black'}}><input*/}
                                    {/*    onChange={this.handleChange.bind(this)} placeholder="הקלד..."*/}
                                    {/*    style={{width: '100%'}}></input></td>*/}
                                    <td id="fullName"
                                        style={{border: '1px solid black'}}>{this.props.state.obj.fullName}</td>
                                    {/*<td id="id" style={{border: '1px solid black'}}><input*/}
                                    {/*    onChange={this.handleChange.bind(this)} placeholder="הקלד..."*/}
                                    {/*    style={{width: '100%'}}></input></td>*/}
                                    <td id="id" style={{border: '1px solid black'}}>{this.props.state.obj._id}</td>
                                </tr>
                                </tbody>
                            </table>
                            <h5>כולנו ביחד וכל אחד לחוד</h5>

                        </div>
                    </div>
                    <div className="row" style={{paddingBottom: " 1em"}}>
                        <div className="col-md-12">
                            <h5>מסמיכים את</h5>
                            <table style={{border: '1px solid black', width: '50%'}}>
                                <thead>
                                <tr>
                                    <th style={{border: '1px solid black'}}>שם החברה</th>
                                    <th style={{border: '1px solid black'}}>מספר ח.פ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    {/*<td id="companyName" style={{border: '1px solid black'}}><input*/}
                                    {/*    onChange={this.handleChange.bind(this)} placeholder="הקלד..."*/}
                                    {/*    style={{width: '100%'}}></input></td>*/}
                                    <td id="id" style={{border: '1px solid black'}}>צחי כהן</td>
                                    {/*<td id="numHp" style={{border: '1px solid black'}}><input*/}
                                    {/*    onChange={this.handleChange.bind(this)} placeholder="הקלד..."*/}
                                    {/*    style={{width: '100%'}}></input></td>*/}
                                    <td id="id" style={{border: '1px solid black'}}>123456789</td>
                                </tr>
                                </tbody>
                            </table>
                            <h5>כולם יחד וכל אחד לחוד, שיקראו להלן: "המוסמכים"</h5>
                        </div>
                    </div>
                </div>
                <div>
                    <div>
                        לפעול בשמנו מטעמנו ובמקומנו בכל הקשור עם הליך בקשה לקבלת הלוואת משכנתה מבנק הפועלים בע"מ, בין
                        הלוואה חדשה ובין הלוואה המיועדת לסילוק הלוואת משכנתה קיימת (להלן: "ההלוואה"), ובכלל זה :

                    </div>
                    '
                    <section id="textarea">
                        <ol>
                            <li>לנהל בשמנו ובמקומנו משא ומתן לצורך קבלת ההלוואה.</li>
                            <li>לפנות אל נציג הבנק, להופיע בפניו, להיפגש עימו ולדון בתנאי ההלוואה השונים, לרבות, בין
                                היתר, גובה הריבית, סוגי ההלוואה, תקופתה וכו'.
                            </li>
                            <li>לקבל מכל מקור שהוא, כולל מהבנק בו מוגשת בקשת ההלוואה, וכן מכל חברה או מעביד, כל מידע שיש
                                בידיהם מכל מקור שהוא, ואשר ייראה למוסמכים על ידינו כדרוש לדיון בבקשת ההלוואה ו/או דרוש
                                כדי לאמת את הפרטים בבקשת ההלוואה, וכן להסמיך את הבנק לקבל מידע זה, ולוותר לצורך כך, על
                                סודיות המידע.
                                הננו מוותרים על זכויותינו לסודיות או לאחריות הבנק ביחס למידע המפורט לעיל, על-פי כל הסכם,
                                נוהג בנקאי ו/או דין, לרבות חוק הגנת הפרטיות התשמ"א-1981 ו/או כל חוק אחר.
                            </li>
                            <li>לקבל, לעיין ולצלם את כל המסמכים הדרושים מגופים שונים, לרבות, בין היתר, אך לא רק, לשכת
                                רישום המקרקעין, רשות מקרקעי ישראל וחברות משכנות, לשם אישור בקשת ההלוואה ולשם ביצועה,
                                לרבות מסמכים שקבלתם מחויבת בעמלה/תשלום.
                            </li>
                            <li>להגיש בשמנו ולחתום במקומנו על טופס בקשה לקבלת ההלוואה, להגיש בשמנו את כל המסמכים הנדרשים
                                לצורך קבלת אישור עקרוני מהבנק לבקשת ההלוואה.
                            </li>
                            <li>לחתום בשמנו ולקבל במקומנו את ההודעה על האישור העקרוני שניתן לבקשת ההלוואה, המפרט את כל
                                תנאי ההלוואה, ובין היתר את שיעורי הריבית ומועד שמירתה.
                            </li>
                            <li>לחתום בשמנו ובמקומנו על טופס הסכמת לקוח למסירת נתוני אשראי לפי חוק נתוני אשראי
                                התשע"ו-2016 (להלן: "חוק נתוני אשראי").
                                ידוע לנו כי מתן ההלוואה מותנה בקבלת דו"ח אשראי עדכני, שהבנק יקבל ישירות מלשכת האשראי,
                                בהתאם לחוק נתוני אשראי לגבי כל אחד מהלווים, ושאינו כולל מידע ו/או נתונים המטילים ספק
                                בדבר יכולתנו לעמוד בהחזרי ההלוואה, ו/או שאינו תואם את הנתונים שימסרו לבנק על-ידינו ו/או
                                על-ידי המוסמכים הנ"ל.
                                על-מנת לקבל את דו"ח האשראי אנו מסכימים כי המוסמכים הנ"ל יחתמו על טופס הסכמה לפיו אנו
                                נותנים את הסכמתנו לכך שנתוני האשראי לגבי, הכלולים במאגר נתוני האשראי, אשר הקים ומנהל בנק
                                ישראל, ימסרו לבנק לשם התקשרות בעסקת ההלוואה ו/או לשם הבטחת קיומה.

                            </li>
                            <li>לקבל בשמנו ובמקומנו "הודעה בנוגע לקבלת חיווי אשראי" בהתאם לחוק נתוני אשראי.
                            </li>
                            <li>אנו מצהירים כי איננו בגדר "לקוחות מוגבלים" ואין לנו "חשבון מוגבל" כמשמעותם בחוק שיקים
                                ללא כיסוי התשמ"א-1981 ומסמיכים את המוסמכים להצהיר זאת בשמנו כלפי הבנק.
                            </li>
                            <li>אנו מצהירים כי לא הוטלו עיקולים כלשהם על הנכס המוצע כשעבוד להבטחת הלוואת המשכנתא ו/או על
                                כספים ו/או על נכסים אחרים שלנו ומסמיכים את המוסמכים להצהיר זאת בשמנו כלפי הבנק.
                            </li>
                            <li>
                                המוסמכים יהיו רשאים, להצהיר בשמנו כלפי הבנק, כי ידוע לנו ואנו מסכימים כי אם תאושר בקשת
                                ההלוואה, במלואה או בחלקה:
                                <br></br>
                                <section>
                                    <ul>
                                        <li>קבלת ההלוואה מותנית באישור הבנק ובהתאם לנהליו.
                                        </li>
                                        <li>אישור ההלוואה לא ישמש אלא הבעת נכונות עקרונית, והוא לא יטיל על הבנק כל
                                            התחייבות ו/או אחריות כלשהן למתן ההלוואה, ורק האמור בהסכם הלוואה חתום בינינו
                                            לבין הבנק יחייב את הבנק.
                                        </li>
                                        <li>הבנק יהיה רשאי, בין השאר, לשנות את שיעור הריבית ו/או שיעור התוספת/הפחתה
                                            לבסיס הריבית ו/או כל תנאים אחרים שיאושרו, וזאת מתום תוקף האישור בהתאם לנקוב
                                            בו .
                                        </li>
                                        <li>אישור ההלוואה מותנה בכך כי כל הפרטים שיימסרו לבנק על ידינו, בין ישירות על
                                            ידינו ובין באמצעות המוסמכים, הם נכונים וכל מסמך שהוגש לבנק כאמור הינו אמיתי,
                                            שלם ובר-תוקף.
                                        </li>
                                        <li>ההתקשרות בעסקת ההלוואה מותנית במילוי כל התנאים והדרישות המצוינים במסמכי הבנק
                                            , שנמסר/יימסר לידינו ו/או לידי המוסמכים, וכן אלה שיפורטו באישור בקשת ההלוואה
                                            שנקבל או יקבלו המוסמכים, וכן בהנחיות הגורם המאשר בבנק שיאשר את בקשת ההלוואה.
                                        </li>
                                        <li>ההתקשרות בעסקת ההלוואה מותניית בתשלום כל ההוצאות הנדרשות לשם ביצוע
                                            ההלוואה.
                                        </li>
                                    </ul>
                                </section>
                            </li>
                            <li>אנו מסכימים כי המידע שאנו ו/או המוסמכים ימסרו לבנק לגבינו במסגרת בקשת ההלוואה ייאגר
                                במאגרי המידע של הבנק.
                            </li>
                            <li>אנו מצהירים כי המוסמכים אינם פועלים מטעם הבנק, וכי הבנק אינו אחראי לפעולותיו/הם.
                            </li>
                        </ol>
                    </section>
                    <div>
                        <div>
                            ולראיה באנו על החתום ב__________ ביום _______ בחודש ________ שנת ________
                        </div>
                        <br></br>
                        <div>
                            <u><u>אימות חתימה ע"י עורך דין:</u></u>
                            <br></br>
                            אני מאשר בזאת את חתימת
                            <br></br>
                            <div className="row">
                                <div className="col-md-4">
                                    שם: צחי כהן
                                </div>
                                <div className="col-md-4">
                                    ת.ז: 123456789
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div className="row">
                                <div className="col-md-4">
                                    תאריך:_________
                                </div>
                                <div className="col-md-4">
                                    שם:_________
                                </div>
                                <div className="col-md-4">
                                    חותמת/חתימה:______________
                                </div>
                            </div>

                        </div>
                        <br/>
                        <br/>
                        <div>
                            <u>אימות חתימה ע"י יועץ המשכנתאות החיצוני:</u>
                            <br></br>

                            אני מאשר בזאת את חתימת:
                            <br></br>
                            <div className="row">
                                <div className="col-md-4">
                                    {/*שם:<input id={"fullName"} onChange={this.handleChange.bind(this)}*/}
                                    {/*          placeholder="הקלד..."></input>*/}
                                    שם: צחי כהן
                                </div>
                                <div className="col-md-4">
                                    {/*ת.ז:<input id="id" onChange={this.handleChange.bind(this)}*/}
                                    {/*           placeholder="הקלד..."></input>*/}
                                    ת.ז: 123456789
                                </div>

                            </div>
                            <br/>
                            <br/>
                            <div className="row">
                                <div className="col-md-4">
                                    תאריך:<input id="date" onChange={this.handleChange.bind(this)}
                                                 placeholder="הקלד..."></input>
                                </div>
                                <div className="col-md-4">
                                    {/*שם:<input onChange={this.handleChange.bind(this)} placeholder="הקלד..."></input>*/}
                                    שם :{this.props.state.obj.fullName}
                                </div>
                                <div className="col-md-4">
                                    <div>
                                        <label style={{marginRight: '-5em'}}>
                                            חותמת\חתימה:
                                        </label>
                                        <div className='container_' style={{display: trimmedDataURL ? 'none' : ''}}>
                                            <div className='sigContainer'>
                                                <SignaturePad
                                                    canvasProps={{width: 330, height: 175, className: 'sigCanvas'}}
                                                    ref={(ref) => {
                                                        this.sigPad = ref
                                                    }}/>
                                                <div style={{width: '20.7em'}}>
                                                    <i onClick={this.clear} style={{cursor: 'pointer'}}
                                                       className="fa fa-times" aria-hidden="true"></i>
                                                    <i onClick={this.trim} style={{float: 'left', cursor: 'pointer'}}
                                                       className="fa fa-check" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                        {trimmedDataURL
                                            ?
                                            <div className='container_image'>
                                                <img className="sigImage"
                                                     src={trimmedDataURL}/>
                                            </div>

                                            : null}
                                    </div>

                                </div>
                            </div>
                            <br/>
                            <div className="row" style={{marginLeft: '28em'}}>
                                <div className="col-md-12">
                                    <button onClick={this.handleSubmit.bind(this)} className="btn btn-secondary"
                                            style={{
                                                borderRadius: '1em',
                                                borderColor: '#f7b742',
                                                backgroundColor: '#f7b742',
                                                float: 'left',
                                                'width': '10em',
                                                'fontWeight': 'bold'
                                            }}>שמור
                                    </button>
                                </div>
                            </div>
                            {
                                this.state.resMsg != '' ?
                                    <div className="row">
                                        <div className="col-md-12">
                                            <Alert style={{textAlign: 'center'}} variant="success">
                                                <Alert.Heading>{this.state.resMsg}</Alert.Heading>
                                            </Alert>
                                        </div>
                                    </div> : ''
                            }

                        </div>
                    </div>
                </div>

            </div>
        )
    }


}

const mapStateToProps = state => {
    console.log('mapStateToProps', state);
    return {
        closeModal: state.sitesReducer.closeModal

    }
}

const mapDispatchToProps = disaptch => {
    return {
        close_modal: (lanuchStatus) => {
            disaptch(closeModal(lanuchStatus));
        }

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PowerAttorney_Poalim);












