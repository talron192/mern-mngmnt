import axios from '../../node_modules/axios';


export const getCustomerData = (customerId) => {
    return async (dispatch) => {

        axios.get("http://localhost:4000/customers/getId/" + customerId, {id: customerId})
            .then(res => { // then print response status
                console.log('getCustomerData', res);
                return dispatch({
                    type: "GET_CUSTOMER_DATA",
                    payload: res.data
                })
            })
            .catch(err => {
                console.log(err);
            });

    }
}

function checkIfExist(bankList, bankName) {
    for (let bank of bankList) {
        if (bank.data.bankName == bankName) return true;
    }
    return false;
}

function checkIfBranchExist(bankList, branch , bankName) {
    let index;
    for (let j in bankList) {
        if(bankList[j].data.bankName == bankName) index=j;
        if(bankList[j].branches.length > 0){
            for(let i in bankList[j].branches){
                if(bankList[j].branches[i].branch == branch) return {isExist:true,index:index} ;
            }
        }
    }
    return {isExist:false,index:index};
}

export const getBankList = () => {
    let bank = {data: {}, branches: []};
    let bankList = [];
    return async (dispatch) => {
        axios.get('http://localhost:4000/customers/getBankContact').then(function (res) {
            let i = 0;
            let j = 0;
            let lastLoop =false;
            // for (let i = 0; i <= res.data.length; i++) {
            while (j <= res.data.length - 1) {
                if (i == 0 && j == 0 || (i == j)) j++;
                if(j == res.data.length) j--;
                if(i == res.data.length){
                    break;
                }
                if (res.data[i].bankName == res.data[j].bankName) {
                    if (!checkIfExist(bankList, res.data[i].bankName)) {
                        bank.branches.push({branch:res.data[i].branch,contactName:res.data[i].contactName});
                        bank.data = res.data[i];
                        bankList.push(bank);
                    } else {
                        let BranchStatus = checkIfBranchExist(bankList, res.data[j].branch,res.data[i].bankName);
                        if (BranchStatus.isExist == false) {
                            let index = BranchStatus.index ;
                            bankList[index].branches.push({branch:res.data[j].branch,contactName:res.data[j].contactName});
                        }
                    }
                    j++;
                } else {
                    if ((i == 0 && (i + 1) == j) || j == 0) {
                        if (!checkIfExist(bankList, res.data[i].bankName)) {
                            bank.branches.push({branch:res.data[i].branch,contactName:res.data[i].contactName});
                            bank.data = res.data[i];
                            bankList.push(bank);
                        }
                    }
                    j++;
                }
                if (j > res.data.length - 1) {
                    i++;
                    j = 0;
                    bank = {data: {}, branches: []};
                }
            }
            // }
            console.log('bankList', bankList);
            return dispatch({
                type: "GET_BANK_LIST",
                payload: bankList
                // payload: res.data
            })
        }).catch(function (e) {
            return [];
        })

    }
}


export const getMsg = (msg) => {
    return async (dispatch) => {

        return dispatch({
            type: "GET_MSG",
            payload: msg
        })

    }
}

export const getState = (state) => {
    return async (dispatch) => {
        return dispatch({
            type: 'GET_CUSTOMER_DATA',
        })
    }
}

export const eventISDone = (eventStatus) => {
    return async (dispatch) => {
        return dispatch({
            type: 'EVENT_IS_DONE',
            payload: eventStatus
        })
    }
}

export const closeModal = (toLaunch) => {
    return async (dispatch) => {
        return dispatch({
            type: 'CLOSE_MODAL',
            payload: toLaunch
        })
    }
}

export const fileIsExist = (fileData) => {
    return async (dispatch) => {

        axios.post("http://localhost:4000/customers/fileIsExist/" + fileData.customerId, fileData)
            .then(res => { // then print response status
                console.log('fileIsExist', res);
                return dispatch({
                    type: "FILE_IS_EXIST",
                    payload: res.data
                })
            })
            .catch(err => {
                console.log(err);
            });


    }
}