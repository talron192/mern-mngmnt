import React, {Component} from '../../node_modules/react';
import axios from '../../node_modules/axios';
import Alert from '../../node_modules/react-bootstrap/Alert';
import {Link} from '../../node_modules/react-router-dom';
import ReactTable from "react-table";
import {Switch} from "react-router-dom";
import Modal from "react-modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import EditBankList from "./editBankList";
import {Api} from './Api';
import PowerAttorney_Poalim from '../powerAttorney_templates/PowerAttorney_Poalim';
import "../images/devMsg.gif";



const api = new Api();


const editCustomerStyle = {
    content: {
        width: '80%',
        right:'150px'
    }
};
export default class AdviserFiles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFile: null,
            isSelected: false,
            uploaded: true,
            show: false,
            msg: '',
            filesList:[],
            ModalIsOpen: false,
            isPoalim:false,


        }
    }

    onChangeHandler = event => {
        this.setState({
            selectedFile: event.target.files[0],
            isSelected: true
        })
    }

    onClickHandler = () => {
        const fileData = new FormData();
        if (!this.state.selectedFile) {
            this.setState({
                show: true,
                msg: 'יש להעלות מסמך'
            })
            return;
        }
        fileData.append('file', this.state.selectedFile, this.state.selectedFile.name);
        axios.post("http://localhost:4000/customers/uploadAdviserFile", fileData)
            .then(res => { // then print response status
                this.setState({
                    uploaded: true,
                    show: true,
                    msg: res.data,
                });
                axios.get("http://localhost:4000/customers/getAdviserFiles")
                    .then(res => { // then print response status
                        console.log(res);

                        this.setState({filesList: res.data.filesList});

                    });
            });
    }

    componentDidMount() {
        // this.setState({filesList:api.GetFiles()});
        axios.post("http://localhost:4000/customers/set-gloal-path")
            .then(res => { // then print response status
                console.log(res);
            });
        axios.get("http://localhost:4000/customers/getAdviserFiles")
            .then(res => { // then print response status
                console.log(res);

                this.setState({filesList: res.data.filesList});

            });

    }

    openCreateFile(e,row){
        switch (row._original.key) {
            case 'Poalim':
                this.setState({isPoalim: true});
                break;


        }
        console.log('openCreateFile',e.target.id);
        console.log('openCreateFile row',row);
        this.setState({
            ModalIsOpen:true
        })
    }
    closeEditCustomer=()=>{
        this.setState({
            ModalIsOpen:false
        })
    }

    openFile() {
        return (
            <Modal onRequestClose={this.closeEditCustomer}
                   style={editCustomerStyle}
                   isOpen={this.state.ModalIsOpen}>
                <ModalHeader style={{textAlign:'center'}} onClick={this.closeEditCustomer}>
                    <h4>כתב הרשאה ליועץ</h4>
                    <i style={{cursor: 'pointer'}} className="fa fa-times" aria-hidden="true"></i>
                </ModalHeader>
                {
                    this.state.isPoalim ?
                        <PowerAttorney_Poalim ></PowerAttorney_Poalim> : 'hello'
                }

            </Modal>
        )
    }

    getListFiles() {
        axios.get("http://localhost:4000/customers/getListFiles/" + this.props.match.params.id, { id: this.props.match.params.id })
            .then(res => { // then print response status
                var filesList = this.mergeFilesListAndTimeUpload(res.data.filesList, res.data.filesTimeUpload);
                this.setState({ listOfFiles: filesList });
            })
            .catch(err => {
                console.log(err);
            });
    }

    deleteFile = (path) => {
        console.log('deleteFile', path)
        axios.post("http://localhost:4000/customers/deleteAdviserFile/", {  fileName: path })
            .then(res => { // then print response status
                console.log('res', res);
                this.setState({ filesList: res.data });
                console.log('this.state.listOfFiles', this.state.filesList);

            })
            .catch(err => {
                console.log(err);
            });
    }

    render() {
        const handleDismiss = () => this.setState({show: false});

        const cols = [
            {
                Header: "שם קובץ",
                accessor: "file_name",
                sortable: false,
                style: {
                    textAlign: "center"
                },
                Cell: props => {
                    console.log(props);
                    // let filePath = "/../../adviser_files/" + props.original;
                    let filePath = "file:///./test.xlsx";

                    return (
                        <a style={{ textDecoration: 'none', cursor: 'pointer', color: 'black' }}  target="_blank"  href={filePath}>{props.original}</a>
                    )
                },
                // Cell: ({row}) => {
                //     console.log(row);
                //     return (
                //         <div>
                //             <button
                //                     className="btn btn-secondary"
                //                     id={'file_'+row._original.key}
                //                     style={{
                //                         marginLeft: '1em',
                //                         borderRadius: '1em',
                //                         borderColor: '#f7b742',
                //                         backgroundColor: '#f7b742',
                //                         'fontWeight': 'bold'
                //                     }}
                //                     onClick={e=>this.openCreateFile(e,row)}
                //             ><i className="fa fa-pencil" aria-hidden="true"></i>
                //             </button>
                //             <button  className="btn btn-secondary"
                //                     style={{
                //                         marginLeft: '1em',
                //                         borderRadius: '1em',
                //                         borderColor: '#f7b742',
                //                         backgroundColor: '#f7b742',
                //                         'fontWeight': 'bold'
                //                     }}
                //             ><i className="fa fa-trash" aria-hidden="true"></i>
                //             </button>
                //         </div>
                //     )
                // },
            },
            {
                Header: "פעולות",
                accessor: "actions",
                sortable: false,
                style: {
                    textAlign: "center"
                },
                Cell: ({row}) => {
                    console.log(row);
                    return (
                        <div>
                            <button  className="btn btn-secondary"
                                     style={{
                                         marginLeft: '1em',
                                         borderRadius: '1em',
                                         borderColor: '#f7b742',
                                         backgroundColor: '#f7b742',
                                         'fontWeight': 'bold'
                                     }}
                            ><i onClick={() => this.deleteFile(row._original)} className="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </div>
                    )
                },
            },
        ]
        return (
            <div>
                <div className="container">
                    <div style={{width: '50%', right: '18em'}} className="input-group">
                        <div className="form-group files">
                            <button type="button" className="btn btn-dark" onClick={this.onClickHandler}>העלאה</button>
                        </div>
                        <div className="custom-file">
                            <input type="file" name="file" onChange={this.onChangeHandler}
                                   className="custom-file-input"/>
                            <label
                                className="custom-file-label">{this.state.isSelected == true ? this.state.selectedFile.name : 'בחר מסמך'}</label>
                        </div>
                    </div>
                    {
                        this.state.show === true ?
                            <Alert style={{width: '22em', right: '25em'}} variant="success" onClose={handleDismiss}
                                   dismissible>
                                <Alert.Heading style={{textAlign: 'center'}}>{this.state.msg}</Alert.Heading>
                            </Alert> : ''
                    }

                </div>
                <ReactTable
                    columns={cols}
                    data={this.state.filesList}
                    filterable
                    loading= {false}
                    noDataText={"אין נתונים"}
                    defaultPageSize={10}
                    style={{ fontSize: '20px', width: '80%', marginRight: '15em' }}
                    loading={this.state.loading ? true : false}

                >
                </ReactTable>
                {this.openFile()}
            </div>


        )
    }
}
// export default withGlobalState(TodoList)
