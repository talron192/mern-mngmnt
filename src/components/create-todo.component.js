import React, {Component, useState, useContext} from '../../node_modules/react';
import {connect} from 'react-redux';
import axios from '../../node_modules/axios';
import {Api} from './Api';
import Steps from './step';
import EditTodo from './docs-upload.component';
import './style.css';
import {getBankList} from "../actions/customerActions";

const ActionType = new Api();
const BankList = new Api();
const MatiralStatus = new Api();
const CustomerType = new Api();
const MortgageAadviceList = new Api();
const contactBanksList = new Api();


class CreateTodo extends Component {
    list = [];
    changeColor = false;

    constructor(props) {
        super(props);

        this.state = {

            list: [],
            fullName: '',
            email: '',
            gender: '',
            customer_id: '',
            _id: '',
            date: '',
            age: '',
            issueDate: '',
            phoneNumber: '',
            houseNumber: '',
            fax: '',
            address: {houseAddress: '', city: '', postalCode: '', poBox: ''},
            bankContact: {branchName: '', contact: '', bankName: ''},
            pathFolder: '',
            actionType: '',
            MortgageAdviceType: '',
            matiralStatus: '',
            sourceArrival: '',
            customerType: '',
            otherContactFullName: '',
            otherContactId: '',
            otherContactDate: '',
            toNextStep: false,
            changeStyle: false,
            isMortgageAdvice: false,
            addMoreContactClicked: false,
            failId: false,
            failsIdMsg: '',
            currentStep: 1,
            stepName: 'createCustomer',
            stepValid: true,
            stepsArr: [],
            bankList: [],
            branches: [],
            contactName: '',
            banksLoaded: false,
            loadSpinner: false,
            isDone: false,
            resMsg:'',
            haveError:false

            //isCompleted:false
        }
    }

    selectGender(gender) {
        if (gender === 1) {
            this.setState({gender: 'men'})
        }
        if (gender === 2) {
            this.setState({gender: 'women'})
        }
    }

    handleChange = (e) => {
        console.log(e.target.id);
        console.log(e.target.value);
        if (e.target.id == '_id') {

            this.setState({
                changeStyle: false
            })
        }
        if (e.target.id == 'actionType') {

            let isMortgageAdvice = e.target.value.split("-")[0] == "1" ? true : false;
            this.setState({isMortgageAdvice: isMortgageAdvice});
        }
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    getCalculationAge(birthday) {

        let birthYear = parseInt(birthday.split("-")[0]);
        var d = new Date();
        var cuurentYear = d.getFullYear();
        let age = cuurentYear - birthYear;

        return age.toString();
    }

    handleBankChange = (e) => {
        console.log(e.target.value);
        let bankContact = Object.assign({}, this.state.bankContact);
        switch (e.target.id) {
            case "bankContact.branchName":
                bankContact.branchName = e.target.value;
                this.buildContact(e.target.value, this.state.bankContact.bankName);

                break;

            case "bankContact.contact":
                bankContact.contact = e.target.value;
                break;

            case "bankContact.bankName":
                console.log(e.target.id);
                bankContact.bankName = e.target.value;
                this.buildBranchContact(e.target.value);

                break;
        }
        this.setState({bankContact});
    }

    buildBranchContact = (bankName) => {
        let branches = [];
        console.log(bankName);
        console.log(this.props.bankList);

        for (let bank of this.props.bankList) {
            console.log(bank);

            if (bankName == bank.data.bankName) {
                if (!bank.data.branch) continue;
                for (let i in bank.branches) {
                    branches.push({branch: bank.branches[i].branch});

                }
            }
        }
        console.log(branches);
        this.setState({branches: branches});

    }

    buildContact = (branchName, bankName) => {
        console.log(branchName);
        for (let bank of this.props.bankList) {
            if (bankName == bank.data.bankName) {
                console.log(bank);
                for (let branch of bank.branches) {
                    if (branchName == branch.branch) {

                        this.setState({contactName: branch.contactName});
                    }

                }
            }
        }
    }

    handleAddressChange = (e) => {
        let address = Object.assign({}, this.state.address);

        switch (e.target.id) {
            case "houseAddress":
                address.houseAddress = e.target.value;
                break;

            case "city":
                address.city = e.target.value;
                break;

            case "postalCode":
                address.postalCode = e.target.value;
                break;

            case "poBox":
                address.poBox = e.target.value;
                break;

        }
        this.setState({address});
    }

    addMoreContact = () => {
        this.setState({addMoreContactClicked: true});
    }

    cancelMoreContact = () => {
        this.setState({addMoreContactClicked: false});
    }

    nextButton() {
        console.log(document.getElementsByClassName("step"));
        let currentStep = this.state.currentStep;
        if (document.getElementsByClassName("step").length > 0 && currentStep < document.getElementsByClassName("step").length) {
            return (
                <div className="col-md-6">
                    <button className="btn btn-secondary" style={{
                        borderRadius: '1em',
                        borderColor: '#f7b742',
                        backgroundColor: '#f7b742',
                        float: 'left',
                        'width': '10em',
                        'fontWeight': 'bold'
                    }}
                            onClick={this._next}>הבא
                    </button>
                </div>
            )
        }
        return null;
    }

    saveDataButton() {
        let currentStep = this.state.currentStep;
        // if (currentStep == document.getElementsByClassName("step").length) {
        //     return null
        // }
        return (
            <button className="btn btn-secondary" style={{
                borderRadius: '1em',
                borderColor: '#f7b742',
                backgroundColor: '#f7b742',
                float: 'left',
                'width': '10em',
                'fontWeight': 'bold'
            }} onClick={this.handleSubmit.bind(this)}>שמור
            </button>
        )
    }

    previousButton() {
        let currentStep = this.state.currentStep;

        return (
            <div className="col-md-6">
                {
                    (currentStep !== 1) ?
                        <button className="btn btn-secondary" style={{
                            borderRadius: '1em',
                            borderColor: '#f7b742',
                            backgroundColor: '#f7b742',
                            float: 'right',
                            'width': '10em',
                            'fontWeight': 'bold'
                        }}
                                onClick={() => this._prev(this.state)}>הקודם
                        </button> : ''
                }
            </div>
        )
        return null;
    }

    stepValidate = (step) => {
        console.log('stepValidate', step);
        switch (step) {

            case 1 :
                let pattern = new RegExp("^\\d{9}$");
                if (!pattern.test(this.state._id)) {
                    console.log('fail id');
                    this.setState({
                        failId: true,
                        failsIdMsg: 'חובה 9 ספרות',
                        haveError:true
                    })
                }
                if (this.state._id == '' || this.state.actionType == '' || this.state.email == '') {
                    this.setState({
                        toNextStep: false,
                        failId: true,
                        failsIdMsg: '',
                        changeStyle: true,
                        haveError:true
                    });
                } else {
                    this.setState({
                        changeStyle: false,
                        toNextStep: true,
                        haveError:false

                    });
                }
                break;
            case 2 :
                break;
        }
    }

    _prev = (state) => {
        let currentStep = this.state.currentStep;
        this.stepValidate(currentStep);
        let nextStep = this.state.toNextStep;
        if (nextStep) {
            currentStep = currentStep <= 1 ? 1 : currentStep - 1;

        }
        this.setState({
            currentStep: currentStep, state: state
        });
    }

    _next = () => {
        let currentStep = this.state.currentStep;
        this.stepValidate(currentStep);
        let nextStep = this.state.toNextStep;
        if (nextStep) {
            currentStep = currentStep >= 2 ? 3 : currentStep + 1;
        }
        this.setState({
            currentStep: currentStep
        })
    }

    clearCurrentClass(currentStep) {
        if (document.getElementsByClassName("step").length <= 0) return;
        let steps = document.getElementsByClassName("step");
        for (let i = 0; i <= steps.length; i++) {
            if (i + 1 == currentStep) continue;
            if (document.getElementById("step_" + (i + 1).toString())) {

                let element = document.getElementById("step_" + (i + 1).toString());
                element.classList.remove("current");
            }
        }
    }

    Step1(currentStep, state) {
        console.log(this.props.bankList);

        if (document.getElementsByClassName("step").length > 0 && currentStep == 1) {
            document.getElementsByClassName("step")[0].className += " current";
            let step = document.getElementsByClassName("step")[0];
            this.clearCurrentClass(1);
            step.setAttribute("id", "step_1");
        }
        if (currentStep !== 1) {
            return null
        }
        return (
            <div>
                <div className="row">
                    <div className="col-md-4">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="text"
                               value={state.fullName}
                               id="fullName"
                               onChange={this.handleChange.bind(this)} placeholder="שם מלא"></input>
                    </div>
                    <div className="col-md-4">
                        <input className="form-control" value={state._id}
                               style={state.changeStyle == false && state._id == '' ? {boxShadow: '-1px 2px 4px 2px #ccc'} : {boxShadow: ' -1px 2px 4px 2px red'}}
                               type="number" onChange={this.handleChange.bind(this)}
                               id="_id" placeholder="ת.ז"></input>
                        {state.changeStyle == true && state._id == '' ?
                            <label style={{float: 'right', color: 'red', fontWeight: '700'}}>חובה</label> : null}
                        {state.failId ? <label style={{
                            float: 'right',
                            color: 'red',
                            fontWeight: '700'
                        }}>{state.failsIdMsg}</label> : null}
                    </div>
                    <div className="col-md-4">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="date"
                               value={state.date}
                               onChange={this.handleChange.bind(this)}
                               id="date" title="תאריך לידה"></input>
                    </div>
                </div>
                <hr></hr>
                <div className="row">
                    <div className="col-md-4">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}}
                               value={state.email}
                               style={state.changeStyle == false && state.actionType == '' ? {boxShadow: '-1px 2px 4px 2px #ccc'} : {boxShadow: ' -1px 2px 4px 2px red'}}
                               type="text" onChange={this.handleChange.bind(this)}
                               id="email" placeholder="אימייל"></input>
                        {state.changeStyle == true && state.email == '' ?
                            <label style={{float: 'right', color: 'red', fontWeight: '700'}}>חובה</label> : null}

                    </div>
                    <div className="col-md-4">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="date"
                               value={state.issueDate}
                               onChange={this.handleChange.bind(this)}
                               id="issueDate" title="תאריך הנפקת תעודת זהות"></input>
                    </div>
                    <div style={{marginTop: '-1em', marginLeft: '9em', marginRight: '5em'}}
                         className="col=md-2"
                         className="gender-position"
                         className={state.gender === "men" ? "gender-color" : ""}>
                        <label className="gender-font"
                               value={"men"}
                               id="gender"
                               onClick={() => this.selectGender(1)}>
                            <i className="fa fa-male" aria-hidden="true"></i>
                        </label>
                    </div>
                    <div style={{marginTop: '-1em', marginLeft: '5em'}}
                         className="col=md-2"
                         className="gender-position"
                         className={state.gender === "women" ? "gender-color" : ""}>
                        <label className="gender-font"
                               value={"women"}
                               id="gender"
                               onClick={() => this.selectGender(2)
                               }>
                            <i className="fa fa-female" aria-hidden="true"></i>
                        </label>
                    </div>
                </div>
                <hr></hr>
                <div className="row">
                    <div className="col-md-4">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="text"
                               value={state.houseNumber}
                               onChange={this.handleChange.bind(this)}
                               id="houseNumber" placeholder="מספר טלפון נוסף"></input>
                    </div>
                    <div className="col-md-4">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="text"
                               value={state.phoneNumber}
                               onChange={this.handleChange.bind(this)}
                               id="phoneNumber" placeholder="טלפון נייד"></input>
                    </div>
                    <div className="col-md-4">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="text"
                               value={state.fax}
                               onChange={this.handleChange.bind(this)}
                               id="fax" placeholder="פקס"></input>
                    </div>
                </div>
                <hr></hr>
                <div className="row">
                    <div className="col-md-3">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="text"
                               value={state.address.houseAddress}
                               onChange={this.handleAddressChange.bind(this)}
                               id="houseAddress" placeholder="כתובת"></input>
                    </div>
                    <div className="col-md-3">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="text"
                               value={state.address.city}
                               onChange={this.handleAddressChange.bind(this)}
                               id="city" placeholder="עיר"></input>
                    </div>
                    <div className="col-md-3">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="text"
                               value={state.address.postalCode}
                               onChange={this.handleAddressChange.bind(this)}
                               id="postalCode" placeholder="מיקוד"></input>
                    </div>
                    <div className="col-md-3">
                        <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}} type="text"
                               value={state.address.poBox}
                               onChange={this.handleAddressChange.bind(this)}
                               id="poBox" placeholder="ת.ד"></input>
                    </div>
                </div>
                <hr></hr>
                <div className="row">
                    <div className="col=md-3" style={{marginRight: "1em"}}>
                        <select style={{boxShadow: '-1px 2px 4px 2px #ccc'}}
                                style={state.changeStyle == false && state.actionType == '' ? {boxShadow: '-1px 2px 4px 2px #ccc'} : {boxShadow: ' -1px 2px 4px 2px red'}}
                                id="actionType"
                                onChange={this.handleChange.bind(this)} className="drop-down">
                            <option style={{backgroundColor: "lightgrey"}}>סוג פעילות</option>
                            {ActionType.GetTypeAction().map((action) => <option key={action.key}
                                                                                value={action.key + '-' + action.value}>{action.value}</option>)}
                        </select>
                        {state.changeStyle == true && state.actionType == '' ?
                            <label style={{float: 'right', color: 'red', fontWeight: '700'}}>חובה</label> : null}

                    </div>
                    {state.isMortgageAdvice ?

                        <div className="col=md-3" style={{marginRight: "1em"}}>
                            <select style={{boxShadow: '-1px 2px 4px 2px #ccc'}}
                                    style={state.changeStyle == false && state.actionType == '' ? {boxShadow: '-1px 2px 4px 2px #ccc'} : {boxShadow: ' -1px 2px 4px 2px red'}}
                                    id="MortgageAdviceType"
                                    onChange={this.handleChange.bind(this)} className="drop-down"
                                    style={{textIndent: '0em'}}>
                                <option style={{backgroundColor: "lightgrey"}}>סוג משכנתא</option>
                                {MortgageAadviceList.GetMortgageAadviceList().map((type) => <option key={type.key}
                                                                                                    value={type.value}>{type.value}</option>)}
                            </select>
                            {state.changeStyle == true && state.actionType == '' ?
                                <label
                                    style={{float: 'right', color: 'red', fontWeight: '700'}}>חובה</label> : null}

                        </div> : ''
                    }
                    <div className="col=md-3" style={{marginRight: "1em"}}>
                        <select style={{boxShadow: '-1px 2px 4px 2px #ccc'}} id="matiralStatus"
                                onChange={this.handleChange.bind(this)} className="drop-down">
                            <option style={{backgroundColor: "lightgrey"}}>מצב משפחתי</option>
                            {MatiralStatus.GetMatiralStatus().map((matiralStatus) => <option key={matiralStatus.key}
                                                                                             value={matiralStatus.value}>{matiralStatus.value}</option>)}
                        </select>
                    </div>
                    <div className="col=md-3" style={{marginRight: "1em"}}>
                        <select style={{boxShadow: '-1px 2px 4px 2px #ccc'}} id="customerType"
                                onChange={this.handleChange.bind(this)} className="drop-down">
                            <option style={{backgroundColor: "lightgrey"}}>מעמד</option>
                            {CustomerType.GetCustomerType().map((customerType) => <option key={customerType.key}
                                                                                          value={customerType.value}>{customerType.value}</option>)}
                        </select>
                    </div>
                    {/* <div className="col=md-3" style={{ marginRight: "1em" }}>
                        <select style={{ boxShadow: '-1px 2px 4px 2px #ccc' }} id="customerType" onChange={this.handleChange.bind(this)} className="drop-down">
                            <option style={{ backgroundColor: "lightgrey" }}>איש קשר בבנק</option>
                            { this.state.contactBankList.map( (contact) => <option key={contact._id} value={contact.branchNumber}>{contact.bankName}</option>)}
                        </select>
                    </div> */}
                    {/* <div className="col=md-4" style={{ marginRight: "1em" }}>
                        <input className="form-control" style={{ boxShadow: '-1px 2px 4px 2px #ccc' }} type="text" onChange={this.handleChange.bind(this)}
                            id="sourceArrival" placeholder="מקור הגעה"></input>
                    </div> */}
                </div>
                <br/>
                {!state.addMoreContactClicked ? <div className="row">
                    <div style={{textAlign: 'center'}} className="col-md-12">
                        <i style={{fontSize: '2em', color: '#f7b742', cursor: 'pointer'}}
                           onClick={this.addMoreContact.bind(this)} className="fa fa-plus-circle fa-2"
                           aria-hidden="true"></i>
                    </div>
                </div> : ''}
                <hr></hr>
                {
                    state.addMoreContactClicked ?
                        <div>
                            <h5 style={{textAlign: 'center'}}><u>איש קשר נוסף</u></h5>
                            <br/>
                            <div className="row">
                                <div className="col-md-4">
                                    <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}}
                                           type="text" id="otherContactFullName"
                                           onChange={this.handleChange.bind(this)} placeholder="שם מלא"></input>
                                </div>
                                <div className="col-md-4">
                                    <input className="form-control"
                                           style={state.changeStyle == false && state._id == '' ? {boxShadow: '-1px 2px 4px 2px #ccc'} : {boxShadow: ' -1px 2px 4px 2px red'}}
                                           type="number" onChange={this.handleChange.bind(this)}
                                           id="otherContactId" placeholder="ת.ז"></input>
                                    {state.changeStyle == true && state._id == '' ? <label
                                        style={{
                                            float: 'right',
                                            color: 'red',
                                            fontWeight: '700'
                                        }}>חובה</label> : null}
                                </div>
                                <div className="col-md-4">
                                    <input className="form-control" style={{boxShadow: '-1px 2px 4px 2px #ccc'}}
                                           type="date" onChange={this.handleChange.bind(this)}
                                           id="otherContactDate" title="תאריך לידה"></input>
                                </div>
                            </div>
                            <br/>
                            <div className="row">
                                <div className="col-md-12" style={{textAlign: 'center'}}>
                                    <i style={{fontSize: '2em', color: '#f7b742', cursor: 'pointer'}}
                                       onClick={this.cancelMoreContact.bind(this)} className="fa fa-minus-circle"
                                       aria-hidden="true"></i>

                                </div>
                            </div>
                        </div> : ''
                }
                <hr></hr>
            </div>
        );
    }

    getBanks() {
        let result = [];
        axios.get('http://localhost:4000/customers/getBankContact').then(function (res) {
            result = res.data;
            console.log(res.data);
            this.setState({
                banksLoaded: true,
                bankList: res.data
            });
        }).catch(function (e) {
            return [];
        })
    }

    Step2(currentStep, state) {
        if (document.getElementsByClassName("step").length > 0 && currentStep == 2) {
            document.getElementsByClassName("step")[1].className += " current";
            let step = document.getElementsByClassName("step")[1];
            this.clearCurrentClass(2);
            step.setAttribute("id", "step_2");
        }
        if (currentStep !== 2) {
            return null
        }
        return (

            <div className="container-fluid" style={{paddingBottom: "25px"}}>
                <h1 style={{textAlign: 'center'}}>בחירת סניף</h1>
                <br/>
                <div className="row" style={{textAlign: 'center'}}>

                    <div className="col-md-4">
                        <select style={{boxShadow: '-1px 2px 4px 2px #ccc'}}
                                id="bankContact.bankName"
                                onChange={this.handleBankChange.bind(this)} className="drop-down">
                            <option value='' style={{backgroundColor: "lightgrey"}}>בחר בנק</option>
                            {this.props.bankList.map((bank) =>
                                <option key={bank.data._id}
                                        value={bank.data.bankName}>{bank.data.bankName}</option>
                            )}
                        </select>
                    </div>

                    <div className="col-md-4">
                        {
                            this.state.bankContact.bankName != '' && this.state.branches.length > 0 ?
                                <select style={{boxShadow: '-1px 2px 4px 2px #ccc'}}
                                        id="bankContact.branchName"
                                        onChange={this.handleBankChange.bind(this)} className="drop-down">
                                    <option value='' style={{backgroundColor: "lightgrey"}}>בחר סניף</option>
                                    {this.state.branches.map((branch) =>
                                        <option key={branch.branch}
                                                value={branch.branch}>{branch.branch}</option>
                                    )}
                                </select> : <h5>לא הוגדרו סניפים</h5>
                        }
                    </div>
                    <div className="col-md-4">

                        {
                            this.state.bankContact.branchName != '' ?

                                <h5>נציג מטפל : {this.state.contactName}</h5> : <h5>אין נציג מטפל</h5>
                        }
                    </div>
                </div>
            </div>
        )
    }

    Step3(currentStep, state) {
        if (document.getElementsByClassName("step").length > 0 && currentStep == 3) {
            document.getElementsByClassName("step")[2].className += " current";
            let step = document.getElementsByClassName("step")[2];
            this.clearCurrentClass(3);
            step.setAttribute("id", "step_3");
        }

        if (currentStep !== 3) {
            return null
        }
        return <EditTodo isRegister={true} id={this.state._id}/>
    }

    handleSubmit = (e) => {

        console.log('this.state.currentStep', this.state.currentStep);
        this.stepValidate(this.state.currentStep);
        const newCustomer = {
            fullName: this.state.fullName,
            email: this.state.email,
            gender: this.state.gender,
            customer_id: this.state.list.length + 1,
            _id: this.state._id,
            date: this.state.date,
            age: this.getCalculationAge(this.state.date),
            issueDate: this.state.issueDate,
            houseNumber: this.state.houseNumber,
            phoneNumber: this.state.phoneNumber,
            fax: this.state.fax,
            actionType: this.state.actionType,
            mortgageAdviceType: this.state.MortgageAdviceType,
            matiralStatus: this.state.matiralStatus,
            sourceArrival: this.state.sourceArrival,
            customerType: this.state.customerType,
            anotherContact: {
                otherContactFullName: this.state.otherContactFullName,
                otherContactId: this.state.otherContactId,
                otherContactDate: this.state.otherContactDate
            },
            address: {
                houseAddress: this.state.address.houseAddress,
                city: this.state.address.city,
                postalCode: this.state.address.postalCode,
                poBox: this.state.address.poBox,
            },
            bankContact: {
                branchName: this.state.bankContact.branchName,
                contact: this.state.contactName,
                bankName: this.state.bankContact.bankName
            },

            pathFolder: 'public/uploads/' + this.state._id,
            contactBankList: []
        };
        // if(this.state.haveError) return;
        this.setState({loadSpinner: true});
        console.log('newCustomer', newCustomer);
        axios.post('http://localhost:4000/customers/add', newCustomer)
            .then(res => {

                console.log('after then response', res.data);
                this.setState({loadSpinner: false});
                this.setState({isDone: true});
                this.props.history.push('/edit/'+this.state._id);


            })
            .catch(err => {
                console.log(err);
            });
    }

    componentDidMount() {
        this.props.get_bank_list();
        axios.get('http://localhost:4000/customers/get')
            .then(res => {
                this.setState({list: res.data});

            })
            .catch(function (err) {
                console.log('error-componentMount', err);
            });

    }

    render() {

        return (

            < div className="App" style={{direction: "rtl", width: '80%', marginRight: '18em'}}>
                <header className="App-header">
                    <Steps/>
                    <div className="row">
                        <div className="col-md-6">
                            <h3 style={{textAlign: 'initial'}}>רישום לקוח</h3>
                        </div>
                        <div className="col-md-6">
                            {this.saveDataButton()}
                        </div>
                    </div>
                    {/*{*/}
                    {/*    this.state.loadSpinner ?*/}
                    {/*        <div className="row spinner-grid">*/}
                    {/*            <div className="col-md-12">*/}
                    {/*                <i className="fa fa-refresh fa-spin"></i>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*        : ''*/}
                    {/*}*/}
                    {/*{*/}
                    {/*    this.state.isDone ?*/}
                    {/*        <div className="modal fade bd-example-modal-sm" tabIndex="-1" role="dialog"*/}
                    {/*             aria-labelledby="mySmallModalLabel" aria-hidden="true">*/}
                    {/*            <div className="modal-dialog modal-sm">*/}
                    {/*                <div className="modal-content">*/}
                    {/*                    נשמר בהצלחה !*/}
                    {/*                </div>*/}
                    {/*                <div className="modal-footer">*/}
                    {/*                    <button type="button" className="btn btn-secondary" data-dismiss="modal">לטבלת הלקוחות*/}
                    {/*                    </button>*/}
                    {/*                    <button type="button" className="btn btn-primary">לכרטיס הלקוח</button>*/}
                    {/*                </div>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*        : ''*/}
                    {/*}*/}


                    <div className="form-fields" style={{paddingTop: '2em'}}>
                        {this.state.stepValid ? this.Step1(this.state.currentStep, this.state) : ''}
                        {this.state.stepValid && this.state.toNextStep ? this.Step2(this.state.currentStep, this.state) : ''}
                        {this.state.stepValid && this.state.toNextStep ? this.Step3(this.state.currentStep, this.state) : ''}
                    </div>

                </header>
                <div className="row">
                    {this.previousButton()}
                    {this.nextButton()}
                </div>

            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        bankList: state.sitesReducer.bankList

    }
}
const mapDispatchToProps = disaptch => {
    return {
        get_bank_list: () => {
            disaptch(getBankList());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTodo);