import React, {Component} from '../../node_modules/react';
import axios from '../../node_modules/axios';
import ReactTable from "../../node_modules/react-table";
import "../../node_modules/react-table/react-table.css";
import EditBankList from './editBankList.js';
import Modal from 'react-modal';
import ModalHeader from '../../node_modules/react-bootstrap/ModalHeader';
import {Link} from "react-router-dom";


const editCustomerStyle = {
    content: {
        width: '68%',
        right: '20%',
    }
};

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

export default class BankList extends Component {
    state = {
        date: new Date(),
        ModalIsOpen: false,
        loading: false,
        list: [],
        clickType: '',
        rowData: {}

    }

    onChange = date => this.setState({date})
    onClickDay = day => {
        console.log('day', day);
        console.log('day type', typeof day);
        this.setState({ModalIsOpen: true});
    }


    openModalClicked = (type, rowData) => {
        console.log('type', type);
        this.setState({
            ModalIsOpen: true,
            clickType: type,
            rowData:rowData
            // obj:this.props.customerData
        });
    }
    closeEditCustomer = () => {
        this.getBanksContacts();
        this.setState({
            ModalIsOpen: false,
            // obj:this.props.customerData
        });
    }

    handleCheckClick(e) {
        console.log(e);
    }


    openModal() {
        return (
            <Modal onRequestClose={this.closeEditCustomer}
                   style={editCustomerStyle}
                   isOpen={this.state.ModalIsOpen}>
                <ModalHeader onClick={this.closeEditCustomer}>
                    <h4>עריכת איש קשר בבנק</h4>
                    <i style={{cursor: 'pointer'}} className="fa fa-times" aria-hidden="true"></i>
                </ModalHeader>
                <EditBankList rowData={this.state.rowData} clickType={this.state.clickType}/>
            </Modal>
        )
    }

    componentDidMount() {
        this.getBanksContacts();
    }

    deleteBankContact(_id){
        console.log(_id);
        this.setState({loading: true});
        axios.post('http://localhost:4000/customers/deleteBankContact/'+_id,{id:_id})
            .then(res => {
                console.log(res.data);
                this.getBanksContacts();
                // this.setState({list: res.data, loading: false});
            })
            .catch((err) => {
                console.log(err);
            });
    }
    getBanksContacts() {
        this.setState({loading: true});
        axios.get('http://localhost:4000/customers/getBankContact')
            .then(res => {
                console.log(res.data);
                let arr = [];
                this.setState({list: res.data, loading: false});
            })
            .catch((err) => {
                console.log(err);
            });
    }

    render() {

        const cols = [
            {
                Header: "עריכה",
                accessor: "checkBank",
                sortable: false,
                style: {
                    textAlign: "center"
                },
                Cell: props => {
                    console.log(props);
                    return (
                        <div>
                            <button onClick={this.openModalClicked.bind(this, 'edit', props.original)}
                                    className="btn btn-secondary"
                                    style={{
                                        marginLeft: '1em',
                                        borderRadius: '1em',
                                        borderColor: '#f7b742',
                                        backgroundColor: '#f7b742',
                                        'fontWeight': 'bold'
                                    }}
                            ><i className="fa fa-pencil" aria-hidden="true"></i>
                            </button>
                            <button onClick={()=>this.deleteBankContact(props.original._id)} className="btn btn-secondary"
                                    style={{
                                        marginLeft: '1em',
                                        borderRadius: '1em',
                                        borderColor: '#f7b742',
                                        backgroundColor: '#f7b742',
                                        'fontWeight': 'bold'
                                    }}
                            ><i className="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </div>


                    )
                },
            },
            {
                Header: "שם בנק",
                accessor: "bankName",
                sortable: false,
                style: {
                    textAlign: "center"
                }
            },
            {
                Header: "מספר סניף",
                accessor: "branch",
                sortable: false,
                style: {
                    textAlign: "center"
                }
            },
            {
                Header: "כתובת",
                accessor: "bankAddress",
                style: {
                    textAlign: "center"
                }
            },
            {
                Header: "נציג מטפל",
                accessor: "contactName",
                style: {
                    textAlign: "center"
                }
            },
            {
                Header: "טלפון",
                accessor: "contactPhone",
                style: {
                    textAlign: "center"
                }
            },
            {
                Header: "כתובת מייל",
                accessor: "contactMail",
                style: {
                    textAlign: "center"
                }
            },
            {
                Header: "שעות פעילות",
                accessor: "activeTime",
                style: {
                    textAlign: "center"
                }
            },
        ]
        return (

            <div>
                <div className="row">
                    <div style={{textAlign: 'center', paddingBottom: '1em'}} className="col-md-12">
                        <button onClick={this.openModalClicked.bind(this, 'add')} className="btn btn-secondary" style={{
                            marginLeft: '1em',
                            borderRadius: '1em',
                            borderColor: '#f7b742',
                            backgroundColor: '#f7b742',
                            'fontWeight': 'bold'
                        }}
                        >הוספת איש קשר
                        </button>
                    </div>
                </div>
                <div className="row">
                    <ReactTable
                        columns={cols}
                        data={this.state.list}
                        filterable
                        loading={this.state.loading ? true : false}
                        noDataText={"אין נתונים"}
                        defaultPageSize={10}
                        style={{fontSize: '20px', width: '80%', marginRight: '15em'}}
                    >
                    </ReactTable>
                </div>
                {
                    this.openModal()
                }


            </div>


        )
    }
}