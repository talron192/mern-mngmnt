import React, {Component} from 'react';
// import MaterialTable from "../../node_modules/material-table";
import "../../node_modules/react-table/react-table.css";
import Alert from '../../node_modules/react-bootstrap/Alert';
import store from '../store.js';
import axios from 'axios';
import {connect} from 'react-redux';
import {eventISDone, getBankList} from '../actions/customerActions';
import {getCustomerData} from '../actions/customerActions';


import './style.css';


class EditCustomerModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            fullName: '',
            email: '',
            gender: '',
            customer_id: '',
            _id: '',
            date: '',
            age: '',
            issueDate: '',
            phoneNumber: '',
            houseNumber: '',
            fax: '',
            address: {houseAddress: '', city: '', postalCode: '', poBox: ''},
            actionType: '',
            MortgageAdviceType: '',
            matiralStatus: '',
            sourceArrival: '',
            customerType: '',
            isSucsses: false,
            bankContact: {branchName: '', contact: '', bankName: ''},
            branches: [],
            branchList: [],
            bankList: [],
            contactName: '',


        }
    }


    updateCustomerData = e => {
        let obj = this.state;
        let propToUpdate = {};
        for (let key in obj) {
            // console.log('obj[key]',obj[key]);
            // console.log('key',key);
            if (obj[key] != "") {
                propToUpdate[key] = obj[key];
            }
        } //updateCustomer
        axios.post("http://localhost:4000/customers/updateCustomer/" + this.props.customer._id, propToUpdate)
            .then(res => { // then print response status
                console.log('res', res);

                this.setState({isSucsses: true});
                this.props.event_is_done(true);
                this.props.get_Customer_Data(this.props.customer._id);
            })
            .catch(err => {
                console.log(err);
            });
    }

    handleBankChange = (e) => {
        console.log(e.target.value);
        let bankContact = Object.assign({}, this.state.bankContact);
        switch (e.target.id) {
            case "bankContact.branchName":
                bankContact.branchName = e.target.value;
                this.buildContact(e.target.value);

                break;

            case "bankContact.contact":
                console.log(e.target.id);
                console.log(e.target.value);
                bankContact.contact = e.target.value;
                break;

            case "bankContact.bankName":
                bankContact.bankName = e.target.value;
                this.buildBranchContact(e.target.value);

                break;
        }
        this.setState({bankContact});
    }

    buildBranchContact = (bankName) => {
        let branches = [];
        console.log(bankName);
        console.log(this.props.bankList);
        for (let bank of this.props.bankList) {
            if (bankName == bank.data.bankName) {
                this.setState({branches: bank.branches});

            }
        }
        console.log(branches);

    }

    buildContact = (branchName) => {
        for (let branch of this.props.bankList) {
            if (branchName == branch.branch) {
                this.setState({contactName: branch.contactName});
            }
        }
    }

    handleAddressChange = (e) => {
        let address = Object.assign({}, this.state.address);

        switch (e.target.id) {
            case "houseAddress":
                address.houseAddress = e.target.value;
                break;

            case "city":
                address.city = e.target.value;
                break;

            case "postalCode":
                address.postalCode = e.target.value;
                break;

            case "poBox":
                address.poBox = e.target.value;
                break;

        }
        this.setState({address});
    }

    handleChange = e => {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    editableForm() {
        console.log('editableForm', this.props.customer);


        return (
            <div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">שם
                        הלקוח</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" onChange={this.handleChange.bind(this)}
                               id="fullName" placeholder={this.props.customer.fullName}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">תאריך הנפקת
                        ת.ז</label>
                    <div className="col-sm-4">
                        <input type="date" className="form-control form-control-lg" id="issueDate"
                               onChange={this.handleChange.bind(this)} placeholder={this.props.customer.issueDate}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">תאריך
                        לידה</label>
                    <div className="col-sm-4">
                        <input type="date" className="form-control form-control-lg" id="date"
                               onChange={this.handleChange.bind(this)} placeholder={this.props.customer.date}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">טלפון
                        בית</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="houseNumber"
                               onChange={this.handleChange.bind(this)} placeholder={this.props.customer.houseNumber}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">טלפון
                        נייד</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="phoneNumber"
                               onChange={this.handleChange.bind(this)} placeholder={this.props.customer.phoneNumber}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">פקס</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="fax" onChange={this.handleChange.bind(this)}
                               placeholder={this.props.customer.fax}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">אימייל</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="email"
                               onChange={this.handleChange.bind(this)} placeholder={this.props.customer.email}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">רחוב</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="houseAddress"
                               onChange={this.handleAddressChange.bind(this)}
                               placeholder={this.props.customer.houseAddress}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">עיר</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="city"
                               onChange={this.handleAddressChange.bind(this)}
                               placeholder={this.props.customer.address.city}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">מיקוד</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="postalCode"
                               onChange={this.handleAddressChange.bind(this)}
                               placeholder={this.props.customer.address.postalCode}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">ת.ד</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="poBox"
                               onChange={this.handleAddressChange.bind(this)}
                               placeholder={this.props.customer.address.poBox}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">סוג
                        פעילות</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="actionType"
                               onChange={this.handleChange.bind(this)} placeholder={this.props.customer.actionType}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">מצב
                        משפחתי</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="matiralStatus"
                               onChange={this.handleChange.bind(this)} placeholder={this.props.customer.matiralStatus}/>
                    </div>
                </div>

                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">גיל</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="age" onChange={this.handleChange.bind(this)}
                               placeholder={this.props.customer.age}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">אישר קשר-שם
                        מלא</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="anotherContact.otherContactFullName"
                               onChange={this.handleChange.bind(this)}
                               placeholder={this.props.customer.hasOwnProperty('anotherContact') ? this.props.customer.anotherContact.otherContactFullName : ''}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">איש
                        קשר-ת.ז</label>
                    <div className="col-sm-4">
                        <input className="form-control form-control-lg" id="anotherContact.otherContactId"
                               onChange={this.handleChange.bind(this)}
                               placeholder={this.props.customer.hasOwnProperty('anotherContact') ? this.props.customer.anotherContact.otherContactId : ''}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">איש קשר-תאריך
                        לידה</label>
                    <div className="col-sm-4">
                        <input type="date" className="form-control form-control-lg" id="anotherContact.otherContactDate"
                               onChange={this.handleChange.bind(this)}
                               placeholder={this.props.customer.hasOwnProperty('anotherContact') ? this.props.customer.anotherContact.otherContactDate : ''}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">בנק
                        מטפל</label>
                    <div className="col-sm-4">
                        <select selec style={{boxShadow: '-1px 2px 4px 2px #ccc', textIndent: '7em', width: '19em'}}
                                id="bankContact.bankName"
                                onChange={this.handleBankChange.bind(this)} className="drop-down">
                            {this.props.bankList.map((bank) =>
                                <option key={bank.data._id} selected={this.setDefaultBank(bank) ? 'selected' : ''}
                                        value={bank.data.bankName}>{bank.data.bankName}</option>
                            )}
                        </select>
                        {/*<input type="text" className="form-control form-control-lg" id="bankContact.bankName"*/}
                        {/*       onChange={this.handleChange.bind(this)}*/}
                        {/*       placeholder={this.props.customer.hasOwnProperty('bankContact') ? this.props.customer.bankContact.bankName : ''}/>*/}
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">סניף
                        מטפל</label>
                    <div className="col-sm-4">
                        {
                            this.state.branches.length == 0 ?
                                this.setBranchList() : this.setSpecificBranchList()
                        }
                        {/*<input type="text" className="form-control form-control-lg" id="bankContact.branchName"*/}
                        {/*       onChange={this.handleChange.bind(this)}*/}
                        {/*       placeholder={this.props.customer.hasOwnProperty('bankContact') ? this.props.customer.bankContact.branchName : ''}/>*/}
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">איש קשר מטפל
                        בסניף</label>
                    <div className="col-sm-4">
                        <input type="text" className="form-control form-control-lg" id="bankContact.contact"
                               onChange={this.handleBankChange.bind(this)}
                               placeholder={this.props.customer.hasOwnProperty('bankContact') ? this.props.customer.bankContact.contact : ''}/>
                    </div>
                </div>
                <div style={{textAlign: 'center'}} className="col-auto">
                    <button onClick={this.updateCustomerData.bind(this)} className="btn btn-primary mb-2">עדכן</button>
                </div>
                {
                    this.state.isSucsses ?
                        <Alert style={{width: '22em', right: '25em'}} variant="success">
                            <Alert.Heading>עודכן בהצלחה</Alert.Heading>
                        </Alert> : ''

                }
            </div>


        )
    }

    setSpecificBranchList() {
        console.log('this.state.branches', this.state.branches);
        return (
            <select style={{boxShadow: '-1px 2px 4px 2px #ccc', textIndent: '7em', width: '19em'}}
                    id="bankContact.branchName"
                    onChange={this.handleBankChange.bind(this)} className="drop-down">
                {
                    this.state.branches.map((branch) =>
                        <option key={branch.branch}
                                value={branch.branch}>{branch.branch}</option>
                    )
                }
            </select>
        )
    }

    setBranchList() {
        console.log(this.props.bankList);
        console.log(this.props.customer.bankContact.bankName);
        let branchList = [];
        for (let bank of this.props.bankList) {
            if (bank.data.bankName == this.props.customer.bankContact.bankName) {
                branchList = bank.branches;
            }
        }
        console.log('branchList', branchList);

        return (
            <select style={{boxShadow: '-1px 2px 4px 2px #ccc', textIndent: '7em', width: '19em'}}
                    id="bankContact.branchName"
                    onChange={this.handleBankChange.bind(this)} className="drop-down">
                {
                    branchList.map((branch) =>
                        <option key={branch.branch} selected={this.setDefaultBranch(branch) ? 'selected' : ''}
                                value={branch.branch}>{branch.branch}</option>
                    )
                }
            </select>
        )
        return (<h1>test</h1>)
        // for (let bank of this.props.bankList) {
        //     if (bank.data.bankName == this.props.customer.bankContact.bankName) {
        //         // return bank.branches;
        //         this.setState({branchList: bank.branches});
        //         return (
        //             <select style={{boxShadow: '-1px 2px 4px 2px #ccc', textIndent: '7em', width: '19em'}}
        //                     id="bankContact.branchName"
        //                     onChange={this.handleBankChange.bind(this)} className="drop-down">
        //                 {
        //                     bank.branches.map((branch) =>
        //                         <option key={branch.branch} selected={this.setDefaultBranch(branch) ? 'selected' : ''}
        //                                 value={branch.branch}>{branch.branch}</option>
        //                     )
        //                 }
        //             </select>
        //
        //         )
        //     }
        // }
    }

    setDefaultBank(bank) {
        console.log(this.props.bankList);
        if (this.props.customer.bankContact.bankName == bank.data.bankName) {

            return true;
        }
        return false;
    }

    setDefaultBranch(branch) {
        console.log(this.state.branches);
        if (this.props.customer.bankContact.branchName == branch.branch) return true;
        return false;
    }


    componentDidMount() {
        this.props.get_bank_list();
        this.setBranchList();

    }


    render() {

        return (
            this.editableForm()
        )
    }


}

const mapStateToProps = state => {
    return {
        bankList: state.sitesReducer.bankList

    }
}

const mapDispatchToProps = disaptch => {
    return {

        event_is_done: (eventStatus) => {
            console.log(eventStatus);
            disaptch(eventISDone(eventStatus));
        },
        get_Customer_Data: (id) => {
            console.log(id);
            disaptch(getCustomerData(id));
        },
        get_bank_list: () => {
            disaptch(getBankList());
        }


    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditCustomerModal);

