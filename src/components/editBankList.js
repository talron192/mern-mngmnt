import React, { Component } from 'react';
// import MaterialTable from "../../node_modules/material-table";
import "../../node_modules/react-table/react-table.css";
import Alert from 'react-bootstrap/Alert';
import store from '../store.js';
import axios from 'axios';
import { connect } from 'react-redux';
import { eventISDone } from '../actions/customerActions';
import { getCustomerData } from '../actions/customerActions';
import './style.css';



export default class EditBankList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      bankName: '',
      branch: '',
      bankContact: { phoneNumber: '', bankContactName: '', email: '' },
      isSucsses: false

    }
  }

  updateContact = e => {
    let obj = this.state;
    let propToUpdate = {};
    for (let key in obj) {
      // console.log('obj[key]',obj[key]);
      // console.log('key',key);
      if (obj[key] != "") {
        propToUpdate[key] = obj[key];
      }
    } //updateCustomer
    console.log(propToUpdate);
    axios.post("http://localhost:4000/customers/updateBankContact/" + this.props.rowData._id, propToUpdate)
        .then(res => { // then print response status
          console.log('res', res);

          this.setState({isSucsses:true});
        })
        .catch(err => {
          console.log(err);
        });
  }

  addContact = e => {
    console.log(this.state);
    const bankContact = {
      bankName: this.state.bankName,
      branch: this.state.branch,
      contactName: this.state.contactName,
      contactPhone: this.state.contactPhone,
      contactMail: this.state.contactMail,
      bankAddress: this.state.bankAddress,
      activeTime: this.state.activeTime,
      bankContact: {
        phoneNumber: this.state.bankContact.phoneNumber,
        bankContactName: this.state.bankContact.bankContactName,
        email: this.state.bankContact.email
      }
    }
    axios.post('http://localhost:4000/customers/addBankContact', bankContact)
      .then(res => {
        this.setState({isSucsses:true});
        console.log('after then response', res.data);
      })
      .catch(err => {
        console.log(err);
      });

  }

  setPlaceHolderValue = e=>{
    console.log('setPlaceHolderValue',e.target.id);
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  }
  handleContactChange = e => {
    let bankContact = Object.assign({}, this.state.bankContact);

    switch (e.target.id) {

      case 'bankContactName':
        bankContact.bankContactName = e.target.value;
        break;
      case 'phoneNumber':
        bankContact.phoneNumber = e.target.value;
        break;
      case 'email':
        bankContact.email = e.target.value;
        break;
    }
    this.setState({ bankContact });
  }
  editableForm() {
    console.log('editableForm', this.props.customer);
    return (
      <div  >
        <div className="form-group row">
          <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">שם הבנק</label>
          <div className="col-sm-4">
            <input placeholder={this.props.rowData.bankName} className="form-control form-control-lg" onChange={this.handleChange.bind(this)} id="bankName" />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">מספר סניף</label>
          <div className="col-sm-4">
            <input placeholder={this.props.rowData.branch} type="number" className="form-control form-control-lg" id="branch" onChange={this.handleChange.bind(this)} />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">שם איש קשר</label>
          <div className="col-sm-4">
            <input placeholder={this.props.rowData.contactName} type="text" className="form-control form-control-lg" id="contactName" onChange={this.handleChange.bind(this)} />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">טלפון</label>
          <div className="col-sm-4">
            <input placeholder={this.props.rowData.contactPhone} type="number" className="form-control form-control-lg" id="contactPhone" onChange={this.handleChange.bind(this)} />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">אימייל</label>
          <div className="col-sm-4">
            <input placeholder={this.props.rowData.contactMail} type="text" className="form-control form-control-lg" id="contactMail" onChange={this.handleChange.bind(this)} />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">כתובת</label>
          <div className="col-sm-4">
            <input  placeholder={this.props.rowData.bankAddress} type="text" className="form-control form-control-lg" id="bankAddress" onChange={this.handleChange.bind(this)} />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="colFormLabelLg" className="col-sm-2 col-form-label col-form-label-lg">שעות פעילות</label>
          <div className="col-sm-4">
            <input placeholder={this.props.rowData.activeTime}  type="text" className="form-control form-control-lg" id="activeTime" onChange={this.handleChange.bind(this)} />
          </div>
        </div>
        <div style={{ textAlign: 'center' }} className="col-auto">
          <button onClick={ this.props.clickType =='add' ?this.addContact.bind(this) : this.updateContact.bind(this) } className="btn btn-primary mb-2">עדכן</button>
        </div>
        {
          this.state.isSucsses ?
            <Alert style={{ width: '22em', right: '25em' }} variant="success" >
              <Alert.Heading>עודכן בהצלחה</Alert.Heading>
            </Alert> : ''

        }
      </div>


    )
  }


  componentDidMount() {
    console.log(this.props.rowData);
  }

  render() {

    return (
      this.editableForm()
    )
  }


}




