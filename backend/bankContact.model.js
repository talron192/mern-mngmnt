const mongoose = require('mongoose');
const Shema = mongoose.Schema;

let BankContact= new Shema({
    bankName:{
        type:String
    } ,
    branch:{
        type:String
    } ,
    contactName:{
        type:String
    } ,
    contactPhone:{
        type:String
    } ,
    contactMail:{
        type:String
    } ,
    bankAddress:{
        type:String
    } ,
    activeTime:{
        type:String,        
    },
});

module.exports= mongoose.model('bankContact',BankContact);